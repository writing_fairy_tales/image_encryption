function P2 = hundun_jiami( P1,k )
%UNTITLED 此处显示有关此函数的摘要
%   此处显示详细说明

PH1 = arrayfun(@(x) bitget(x,8),P1,'UniformOutput',false);
PH2 = arrayfun(@(x) bitget(x,7),P1,'UniformOutput',false);
PH3 = arrayfun(@(x) bitget(x,6),P1,'UniformOutput',false);
PH4 = arrayfun(@(x) bitget(x,5),P1,'UniformOutput',false);

PL5 = arrayfun(@(x) bitget(x,4),P1,'UniformOutput',false);
PL6 = arrayfun(@(x) bitget(x,3),P1,'UniformOutput',false);
PL7 = arrayfun(@(x) bitget(x,2),P1,'UniformOutput',false);
PL8 = arrayfun(@(x) bitget(x,1),P1,'UniformOutput',false);

PH1 = cell2mat(PH1);
PH2 = cell2mat(PH2);
PH3 = cell2mat(PH3);
PH4 = cell2mat(PH4);

PL5 = cell2mat(PL5);
PL6 = cell2mat(PL6);
PL7 = cell2mat(PL7);
PL8 = cell2mat(PL8);

sz2 = size(P1);
r = sz2(1)*sz2(2);

k1 = k(isstrprop(k,'digit'));
x0 = str2double(k1(1:4))/1000;
y0 = str2double(k1(2:5))/1000;
z0 = str2double(k1(3:6))/1000;
w0 = str2double(k1(4:7))/1000;
e0 = rem(str2double(k1(5:8)),0.798-0.085)+0.085;
A=chen_output(x0,y0,z0,w0,e0,r);   
X=A(:,1);
X=X(3002:length(X));        %去除前3001项，获得更好的随机性（求解陈氏系统的子函数多计算了3000点）
Y=A(:,2);
Y=Y(3002:length(Y));
Z=A(:,3);
Z=Z(3002:length(Z));
W=A(:,4);
W=W(3002:length(W));

[~,Tnx] = sort(X);
[~,Tny] = sort(Y);
[~,Tnz] = sort(Z);
[~,Tnw] = sort(W);
Tnx = reshape(Tnx,[sz2(1),sz2(2)]);
Tny = reshape(Tny,[sz2(1),sz2(2)]);
Tnz = reshape(Tnz,[sz2(1),sz2(2)]);
Tnw = reshape(Tnw,[sz2(1),sz2(2)]);



if length(sz2)==3
    t1_PH1 = PH1(:,:,1);
    t2_PH1 = PH1(:,:,2);
    t3_PH1 = PH1(:,:,3);
    ph1(:,:,1) = t1_PH1(Tnx);
    ph1(:,:,2) = t2_PH1(Tnx);
    ph1(:,:,3) = t3_PH1(Tnx);
    
    t1_PH2 = PH2(:,:,1);
    t2_PH2 = PH2(:,:,2);
    t3_PH2 = PH2(:,:,3);
    ph2(:,:,1) = t1_PH2(Tny);
    ph2(:,:,2) = t2_PH2(Tny);
    ph2(:,:,3) = t3_PH2(Tny);
    
    t1_PH3 = PH3(:,:,1);
    t2_PH3 = PH3(:,:,2);
    t3_PH3 = PH3(:,:,3);
    ph3(:,:,1) = t1_PH3(Tnz);
    ph3(:,:,2) = t2_PH3(Tnz);
    ph3(:,:,3) = t3_PH3(Tnz);
    
    t1_PH4 = PH4(:,:,1);
    t2_PH4 = PH4(:,:,2);
    t3_PH4 = PH4(:,:,3);
    ph4(:,:,1) = t1_PH4(Tnw);
    ph4(:,:,2) = t2_PH4(Tnw);
    ph4(:,:,3) = t3_PH4(Tnw);
    
    t1_PL5 = PL5(:,:,1);
    t2_PL5 = PL5(:,:,2);
    t3_PL5 = PL5(:,:,3);
    pl5(:,:,1) = t1_PL5(Tnx);
    pl5(:,:,2) = t2_PL5(Tnx);
    pl5(:,:,3) = t3_PL5(Tnx);
    
    t1_PL6 = PL6(:,:,1);
    t2_PL6 = PL6(:,:,2);
    t3_PL6 = PL6(:,:,3);
    pl6(:,:,1) = t1_PL6(Tny);
    pl6(:,:,2) = t2_PL6(Tny);
    pl6(:,:,3) = t3_PL6(Tny);
    
    t1_PL7 = PL7(:,:,1);
    t2_PL7 = PL7(:,:,2);
    t3_PL7 = PL7(:,:,3);
    pl7(:,:,1) = t1_PL7(Tnz);
    pl7(:,:,2) = t2_PL7(Tnz);
    pl7(:,:,3) = t3_PL7(Tnz);
    
    t1_PL8 = PL8(:,:,1);
    t2_PL8 = PL8(:,:,2);
    t3_PL8 = PL8(:,:,3);
    pl8(:,:,1) = t1_PL8(Tnw);
    pl8(:,:,2) = t2_PL8(Tnw);
    pl8(:,:,3) = t3_PL8(Tnw);

else
    ph1 = PH1(Tnx);
    ph2 = PH2(Tny);
    ph3 = PH3(Tnz);
    ph4 = PH4(Tnw);
    
    pl5 = PL5(Tnx);
    pl6 = PL6(Tny);
    pl7 = PL7(Tnz);
    pl8 = PL8(Tnw);
end

pl5 = bitxor(pl5,PH4);
pl6 = bitxor(pl6,PH3);
pl7 = bitxor(pl7,PH2);
pl8 = bitxor(pl8,PH1);

ph1 = bitxor(ph1,pl5);
ph2 = bitxor(ph2,pl6);
ph3 = bitxor(ph3,pl7);
ph4 = bitxor(ph4,pl8);
P2 = pl8+pl7*2+pl6*2^2+pl5*2^3+ph4*2^4+ph3*2^5+ph2*2^6+ph1*2^7;

end

